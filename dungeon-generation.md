# Timeless Adventures Dungeon Algorithm (Outline)

After a floor with a specific size (size-corridor depends on the dungeon. Can be anywhere from ~28 by ~28 to ~98 by ~98 fields) was generated, the following steps are done in order:

## 1. Generating rooms

There are two supported ways of generating rooms:

  1. Random generation
  2. With a floor-layout

Which type it is can be set on a per-dungeon-basis.

### Random generation

At first, a number of rooms gets generated. This isn't the fixed number of rooms, but a maximum. It is roughly calculated by taking the width of the floor and dividing it by 7 (average size of a random room is 6 by 6 fields + one extra so that there is space between the rooms). This number is then multiplied with the value gotten by taking the height of the floor and dividing it by 7 (same thing, again).

**Note:** The average room-size can also be set on a per-dungeon-basis, 7 is just the default.

After that, a random number of 0-3 rooms gets subtracted from the maximum (clamping at 3 (or a value set by the dungeon)).

Next, the floor will be split in a grid consisting of 7 by 7 cells (again, this size can be overridden). If there isn't enough space for cells of this size at the right or bottom border, the outer-most cells will be discarded.

Now, for each room in the number of rooms, a random grid will be selected and a room will be reserved there. There are three types of rooms:

  1. Normal rooms
  2. High rooms
  3. Wide rooms

Normal rooms reserve 1-2 grid cells in both height and width.

High and wide rooms are basically the same thing, just flipped on their side. The reserved space for them is 2 grid cells in width (horizontally for high rooms, vertically for wide rooms) and the whole screen in length (vertically for high rooms, horizontally for wide rooms).

There can only be a maximum of 2 high or wide rooms per floor and they can't be mixed. If there is only one high or wide room, it will randomly be placed on either the outer-most 2 columns/rows (either the first 2 or the last 2) or the middle-most ones (if the number of cells is odd, it will be randomly selected if it will tend more to the beginning or the end). If there are 2 high or wide rooms (only possible if the floor height/width is at least 5 grid cells), they will be placed on the first 2 _and_ the last 2 columns/rows.

Before any normal rooms will be reserved, a random number of 0-2 high or wide-rooms will be reserved (only if the floor-size is at least 3 grid cells wide and 4 high (for high floors) or vice-versa (for wide floors)).

Now the rooms will be generated. Each room gets a random size of 3-6 fields per reserved grid-field, in both height and width. It will then be randomly placed inside it's reserved grid-fields, making sure that it never touches the outer-most fields of the outer-most of it's reserved grid-fields. If one of it's borders directly aligns with the border of the same side of another room, there is a 40% chance the room will be moved 1 field, or a 10% chance that it will be moved 2 fields, in any of the two directions that will break this alignment (it will only be moved in a direction if it won't touch the outer-most fields of it's grid-fields afterwards).

### With a floor-layout

A floor-layout doesn't give away the whole layout of the floor, just the grid and the reserved cells for rooms. It contains:

  * The height of a grid-cell
  * The width of a grid-cell
  * The height of the floor in grid-cells
  * The width of the floor in grid-cells
  * A list of rooms consisting of:
    * The type of the room (same three types as above)
    * The x-index of the top-left-most reserved grid-cell
    * The y-index of the top-left-most reserved grid-cell
    * The number of reserved grid-cells for the height (only for normal rooms)
    * The number of reserved grid-cells for the width (only for normal rooms)

After the grid-cells for each room specified in the file are reserved, the algorithm will continue to generate the rooms the same way as with random-generation.

### Notes

After all rooms are generated, an extra 1-2 fields of space will be added between rows and columns, resulting in a space of at least 3 fields between room-borders.

Randomly-generated dungeons will be as playable as dungeons with a floor-layout, although they might look a bit "messy" when compared to original dungeons.

## 2. Generating corridors

For each room, a number of 1-4 exits will be generated (there can only be a maximum of 2 exits per side, with a space of at least 1 field between them. There won't be an exit on a side if it directly faces the border of the floor). Then, a random number (of up to a fifth of the number of rooms, ceiled) of "lone exits" will be generated. They can be generated on any empty grid-cell if there is a room on any of the grid cells adjacent to it. These will be used for dead-ends.

Next, each exit (that is not a lone exit) will be virtually connected with 0-2 other exits (that are either room-exits this exit isn't yet connected with, or lone exits without a connection yet).

After that, the corridors themselves will be generated. For this, the fastest route between two exits will be calculated using any path-finding algorithm (with rooms (+ 1 field on every side) acting as obstacles) During this process, a number of corner-points for the corridor will be generated. Each corner-point will be at last 1 field, if possible 2 or even more fields away from any room-border.

Next, a few (0-4, maximum number depending on the corridor length) extra corner-points will be generated, which vary 1-2 fields from the calculated path. This causes corridors to look more natural, and a bit "broken up". Once this is done, the corners of the corridor will be connected.

## 3. Generating water/lava/holes

Each dungeon specifies a wall-to-water ratio (for example, 70% wall, 30% water).

At first the number of water will be calculated. This happens by taking the number of wall-fields on the current floor and calculating the percentage of water (f.e., if there are 100 wall fields on the floor and there should be 30% water, there will be 30 fields of water). Next, the number of water "sources" will be calculated. For this, the number of water fields will be taken, divided by 4 (arbitrary, can be played around with to get different results), and ceiled.

Now the water sources will be placed. For this, a chance will be calculated for each wall-field on the floor. Every field starts with a chance of 50%. If there is any empty field directly adjacent to it, 10% get added to the chance. If there also is a room on the same grid cell, another 20% get added. With all this being calculated, the number of water sources will be placed, on random wall-fields given the chances calculated before. However, there can only be a maximum of 1 water source per grid cell.

Now, until the number of needed water fields is reached, the following process gets repeated:

  1. Calculate the chance for water for every wall-field (starting with 0%):
    * Every adjacent water-field adds 10%
    * Every adjacent empty-field adds 5%
    * If there is a room on the same grid field (and the chance isn't 0%), another 15% get added
  2. Place water on the field with the highest chance. If there are multiple colliding chances, choose one at random.
