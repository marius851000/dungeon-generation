# dungeongeneration
This is an implementation of a random dungeon generation, based on an algoritm of stunt(hacks), itself based on the level generation of the mystery dungeon serie.

See main.rs for an example of usage. You can also use cargo doc to generate documentation.

Also, please note that it is my first program in rust.
