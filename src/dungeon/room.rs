#[derive(Debug, Copy, Clone)]
/// store data about a room in a dungeon
pub struct Room {
    /// the horizontal coordinate of the left extremity of the room
    pub x: usize,
    /// the vertical coordinate of the downer extremity of the room
    pub y: usize,
    /// the horizontal size of the room
    pub xsize: usize,
    /// the vertical size of the room
    pub ysize: usize,
}
