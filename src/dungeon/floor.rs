//! contain Floor and related function, to store a floor of a dungeon.
use crate::dungeon::tile::{FloorType, Tile};
use crate::dungeon::room::Room;
use crate::common::{Coordinate, Side};
use crate::pathfinding::test_accessibility;
use rand::{rngs::ThreadRng, Rng};
/// Contain floor data ( a level of a dungeon )
/// The zero coordinate are at the down-left
#[derive(Debug)]
pub struct Floor {
    /// the horizontal number of tile of this dungeon
    pub xsize: usize,
    /// the vertical number of tile of this dungeon
    pub ysize: usize,
    /// the first key is x and the second is y
    pub level: Vec<Vec<Tile>>,
    /// the list of [`Room`] this floor contain, is None if uninitialized
    pub rooms: Option<Vec<Room>>,
}

impl Floor {
    /// display the floor in the terminal
    ///
    /// \# are wall, ' ' (space) for floor, ~ for water, = for lava and . for hole
    ///
    /// # Examples
    ///
    /// ```
    /// use dungeongeneration::dungeon::floor::FloorBuilder;
    /// use dungeongeneration::dungeon::tile::FloorType;
    /// let mut floor = FloorBuilder::new(10, 10).initialize();
    /// floor.level[1][1].floor = FloorType::Path;
    /// floor.display()
    /// ```
    pub fn display(&self) {
        for y_reversed in 0..self.ysize {
            let mut l = String::new();
            let y  = self.ysize-y_reversed-1;
            for x in 0..self.xsize {
                l.push_str(
                    match &self.level[x][y].floor {
                        FloorType::Wall => "#",
                        FloorType::Path => " ",
                        FloorType::Water => "~",
                        FloorType::Lava => "=",
                        FloorType::Hole => "."
                    }
                );
            }
            println!("{}", l);
        }
    }
    /// return Some(&Tile) if the tile at the coordinate exist, None otherwise
    pub fn get_tile(&self, coord: &Coordinate) -> Option<&Tile> {
        if coord.x >= self.xsize || coord.y >= self.ysize {
            return None;
        }
        return Some(&self.level[coord.x][coord.y]);
    }
    /// return Some(&Room) if the coordinate correspond to a room, None otherwise
    pub fn get_room_at(&self, coord: &Coordinate) -> Option<&Room> {
        match &self.rooms {
            None => {
                error!("trying to get a room in the level while there are not uninitialized!!! Returning None.");
                return None;
            },
            Some(rooms) => {
                for room in rooms {
                    if coord.x >= room.x && coord.y >= room.y && coord.x < room.x + room.xsize && coord.y < room.y + room.ysize {
                        return Some(room);
                    };
                }
                return None;
            }
        }
    }
    /// place obstacle, replacing [`FloorType::Wall`]
    ///
    /// it take into account the proximity of a [`Room`], that increase the chance of placement, of a [`FloorType::Path`], and of another obstacle of the same type.
    ///
    /// obstacle_type is the [`FloorType`] to place
    /// obstacle_proportion is the proportion (over 1) of [`FloorType::Wall`] to replace with the obstacle
    /// source_proportion is the proportion (over 1) of source. Basically, a greater value should made obstacle more disparate
    pub fn place_obstacle(&mut self, rng: &mut ThreadRng, obstacle_type: FloorType, obstacle_proportion: f64, source_proportion: f64) {
        let mut number_of_wall = 0;
        for x in &self.level {
            for y in x {
                if y.floor == FloorType::Wall {
                    number_of_wall += 1;
                };
            }
        }
        let number_of_obstacle = (number_of_wall as f64 * obstacle_proportion).ceil() as usize;
        let number_of_source = (number_of_obstacle as f64 * source_proportion).ceil() as usize;

        let mut possible_obstacle_source: Vec<Vec<Option<usize>>> = Vec::new();
        let mut sum_of_chance = 0;
        for x in 0..self.xsize {
            possible_obstacle_source.push(Vec::new());
            for y in 0..self.ysize {
                possible_obstacle_source[x].push(match self.level[x][y].floor {
                    FloorType::Wall => {
                        let mut adjacent_to_path = false;
                        let mut adjacent_to_room = false;
                        for side in Side::iterator() {
                            if !side.is_legal(Coordinate{x:x,y:y},self.xsize, self.ysize) {
                                continue;
                            };

                            let actual_coordinate = Coordinate {
                                x: (x as isize + side.x_movement()) as usize,
                                y: (y as isize + side.y_movement()) as usize,
                            };

                            let tile = match self.get_tile(&actual_coordinate) {
                                None => {
                                    error!("tried to access to a off grid tile while computing chance for wall to be an obstacle source!!! ignoring it.");
                                    continue;
                                },
                                Some(value) => value,
                            };

                            if tile.floor == FloorType::Path {
                                adjacent_to_path = true;
                            };

                            match self.get_room_at(&actual_coordinate) {
                                None => {},
                                Some(_) => adjacent_to_room = true,
                            }
                        }
                        let chance = 50 + match adjacent_to_path{false=>0,true=>10} + match adjacent_to_room {false=>0,true=>20};
                        sum_of_chance += chance;
                        Some(chance)
                    },
                    _ => None,
                })
            }
        }

        for _ in 0..number_of_source {
            let choice = rng.gen_range(0, sum_of_chance);
            let mut already_passed_chance = 0;
            let mut found = false;
            let mut source = None;
            for x in 0..self.xsize {
                for y in 0..self.ysize {
                    match possible_obstacle_source[x][y] {
                        None => continue,
                        Some(chance) => {
                            already_passed_chance += chance;
                            if already_passed_chance > choice {
                                found = true;
                                source = Some(Coordinate{x:x,y:y});
                                break;
                            }
                        }
                    }
                }
                if found {break};
            }
            let coord = match source {
                None => {
                    error!("no source found for the choice {} (over a sum of chance of {})!!! skipping it.", choice, sum_of_chance);
                    continue;
                },
                Some(x) => x,
            };
            let chance = match possible_obstacle_source[coord.x][coord.y] {
                None => {
                    error!("an unselectionable possible obstacle source was selectioned!!! skipping it.");;
                    continue;
                },
                Some(x) => x,
            };
            possible_obstacle_source[coord.x][coord.y] = None;
            sum_of_chance -= chance;
            self.level[coord.x][coord.y] = Tile{ floor: obstacle_type, .. Tile::new()};
        }

        for _ in 0..number_of_obstacle-number_of_source {
            let mut pourcentage_of_chance_of_obstacle_by_tile: Vec<Vec<Option<usize>>> = Vec::new();
            let mut max_pourcentage = 0;
            let mut sum_of_chance = 0;
            for x in 0..self.xsize {
                pourcentage_of_chance_of_obstacle_by_tile.push(Vec::new());
                for y in 0..self.ysize {
                    pourcentage_of_chance_of_obstacle_by_tile[x].push(
                        match self.level[x][y].floor {
                            FloorType::Wall => {
                                let mut chance = 0;
                                for side in Side::iterator() {
                                    if !side.is_legal(Coordinate{x:x,y:y},self.xsize, self.ysize) {
                                        continue;
                                    };
                                    let actual_coordinate = Coordinate {
                                        x: (x as isize + side.x_movement()) as usize,
                                        y: (y as isize + side.y_movement()) as usize,
                                    };
                                    match match self.get_tile(&actual_coordinate) {
                                            None => {
                                                error!("tried to access to a non-valid tile coordinate while placing water!!! ignoring it.");
                                                continue;
                                            },
                                            Some(x) => x
                                        }.floor {
                                        FloorType::Path => chance += 5,
                                        x if x == obstacle_type => chance += 10,
                                        _ => {},
                                    };

                                    if chance != 0 {
                                        if self.get_room_at(&actual_coordinate).is_some() {
                                            chance += 15
                                        };
                                    };
                                }
                                if max_pourcentage < chance {
                                    max_pourcentage = chance;
                                };
                                sum_of_chance += chance;
                                Some(chance)
                            },
                            _ => None,
                        }
                    )
                }
            }
            if false {
                let mut possible_best : Vec<Coordinate> = Vec::new();

                for x in 0..self.xsize {
                    for y in 0..self.ysize {
                        match pourcentage_of_chance_of_obstacle_by_tile[x][y] {
                            None => continue,
                            Some(pourcentage) if pourcentage == max_pourcentage => {
                                possible_best.push(Coordinate{x:x,y:y});
                            },
                            _ => continue,
                        };
                    }
                }

                if possible_best.len() == 0 {
                    error!("there are no possible way to place obstacle (not normal)!!! stopping placing obstacle.");
                    return
                }

                let chosen_coordinate = possible_best[rng.gen_range(0, possible_best.len())];
                self.level[chosen_coordinate.x][chosen_coordinate.y].floor = obstacle_type;
            } else {
                let choice = rng.gen_range(0, sum_of_chance);

                let mut already_passed_chance: usize = 0;
                let mut found = false;
                let mut source = None;
                for x in 0..self.xsize {
                    for y in 0..self.ysize {
                        match pourcentage_of_chance_of_obstacle_by_tile[x][y] {
                            None => continue,
                            Some(chance) => {
                                already_passed_chance += chance;
                                if already_passed_chance > choice {
                                    found = true;
                                    source = Some(Coordinate{x:x,y:y});
                                    break;
                                }
                            }
                        }
                    }
                    if found {break};
                }
                let coord = match source {
                    None => {
                        error!("no source found for the choice {} (over a sum of chance of {})!!! skipping it.", choice, sum_of_chance);
                        continue;
                    },
                    Some(x) => x,
                };
                self.level[coord.x][coord.y] = Tile{ floor: obstacle_type, .. Tile::new()};
            }
        }

        // removing unacessible obstacle
        let mut grid: Vec<Vec<bool>> = Vec::new();
        for x in 0..self.xsize {
            grid.push(Vec::new());
            for y in 0..self.ysize {
                grid[x].push(
                    match self.level[x][y].floor {
                        FloorType::Wall => false,
                        _ => true,
                    }
                );
            }
        }

        let rooms = match &self.rooms {
            None => {
                error!("error while trying to unpack the rooms for eliminating uninterrising obstacle!!! returning");
                return;
            },
            Some(x) => x,
        };

        if rooms.len() == 0 {
            error!("error while trying to find an accessible room for eliminiting uninterrising obstacle, no rooms where found !!! returning.");
            return;
        };

        let start = Coordinate{
            x: rooms[0].x+1,
            y: rooms[0].y+1,
        };

        let dead_grid = test_accessibility(&grid, &start);

        for x in 0..self.xsize {
            for y in 0..self.ysize {
                if dead_grid[x][y] == false {
                    if self.level[x][y].floor == obstacle_type {
                        self.level[x][y].floor = FloorType::Wall;
                    }
                };
            }
        }
    }
}

/// allow to easily construct a 'Floor'
///
/// # Examples
///
/// ```
/// use dungeongeneration::dungeon::floor::FloorBuilder;
/// let mut a = FloorBuilder::new(28, 28).initialize();
/// ```
#[derive(Debug, Clone)]
pub struct FloorBuilder {
    pub xsize: usize,
    pub ysize: usize,
}

impl FloorBuilder {
    /// create a new [`FloorBuilder`]
    ///
    /// xsize is the number of horizontal tile
    /// ysize is the number of vertical tile
    pub fn new(xsize: usize, ysize: usize) -> FloorBuilder {
        FloorBuilder { xsize: xsize, ysize: ysize }
    }

    /// generate a [`Floor`] object
    pub fn initialize(&self) -> Floor {
        Floor {
            xsize: self.xsize,
            ysize: self.ysize,
            level: vec![vec![Tile::new(); self.ysize]; self.xsize],
            rooms: None,
        }
    }
}
