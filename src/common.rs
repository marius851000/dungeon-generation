//! contain commons structs and enums
use std::slice::Iter;

#[derive(Debug, Copy, Clone, PartialEq)]
/// represent a coordinate
///
/// in dungeongeneration, the zero point is at the lower-left point of the grid
pub struct Coordinate {
    /// the x coordinate, that grow as it goes up
    pub x: usize,
    /// the y coordinate, that grow as it goes right
    pub y: usize,
}

#[derive(Debug, Copy, Clone, PartialEq)]
/// all possible direction in a 2d grid
pub enum Side {
    Up,
    Down,
    Left,
    Right,
}

impl Side {
    /// return an iterator that iterate over the 4 possible [`Side`]
    pub fn iterator() -> Iter<'static, Side> {
        use self::Side::*;
        [Up, Down, Left, Right].into_iter()
    }
    /// return true if it is a horizontal [`Side`] ([`Side::Up`] and [`Side::Down`]), false otherwise
    pub fn is_horizontal(&self) -> bool {
        match self {
            Side::Up => true,
            Side::Down => true,
            Side::Left => false,
            Side::Right => false,
        }
    }
    pub fn y_movement(&self) -> isize {
        match self {
            Side::Down => -1,
            Side::Up => 1,
            _ => 0,
        }
    }
    pub fn x_movement(&self) -> isize {
        match self {
            Side::Left => -1,
            Side::Right => 1,
            _ => 0,
        }
    }
    /// return false is moving in the direction from the coordinate will go out of the grid, true otherwise.
    pub fn is_legal(&self, coord: Coordinate, xsize: usize, ysize: usize) -> bool {
        match self {
            Side::Up => if coord.y >= ysize-1 { false } else { true },
            Side::Down => if coord.y <= 0 { false } else { true },
            Side::Left => if coord.x <= 0 { false } else { true },
            Side::Right => if coord.x >= xsize-1 { false } else { true },
        }
    }
    /// return the opposite [`Side`]
    pub fn inverse(&self) -> Side {
        match self {
            Side::Left => Side::Right,
            Side::Right => Side::Left,
            Side::Up => Side::Down,
            Side::Down => Side::Up,
        }
    }
}
