//! contain a lot of tool that allow to see a dungeon as a grid of room
extern crate rand;

use crate::pathfinding::find_path;
use crate::common::{Coordinate, Side};
use crate::dungeon::{floor::Floor,
    tile::{FloorType, Tile}};

use crate::dungeon::room::{Room};
use rand::{Rng, rngs::ThreadRng};

#[derive(Debug)]
/// types of rooms
pub enum RoomType {
    /// a room that is a square of size x:(usize), y:(usize)
    Normal(usize),
    /// a room that is a rectangle, of size x:2, y:usize
    High(usize),
    /// a room that is a rectangle, of size x:usize, y:2
    Wide(usize),
}

impl RoomType {

        /// get the horizontal size the room ocuppy in the [`RoomGrid`]
        fn xsize(&self) -> usize {
            match &self {
                RoomType::Normal(size) => *size,
                RoomType::High(_) => 2,
                RoomType::Wide(size) => *size,
            }
        }

        /// get the vertical size the room ocuppy in the [`RoomGrid`]
        fn ysize(&self) -> usize {
            match &self {
                RoomType::Normal(size) => *size,
                RoomType::High(size) => *size,
                RoomType::Wide(_) => 2,
            }
        }
}

#[derive(Debug)]
/// contain a room in the grid
pub struct RoomData {
    /// the horizontal coordinate of the lefter side of the room
    pub x: usize,
    /// the vertical coordinate of the downer side of the room
    pub y: usize,
    /// the type of the room
    pub room_type: RoomType,
    /// the list of door connected to this room
    pub doors: Vec<Door>,
    /// the [`Room`] object, if it generated
    pub applied: Option<Room>,
}

#[derive(Debug)]
/// a door of a room in the grid. It may be connected with up to two others doors
pub struct Door {
    /// the side this door is positioned
    pub side: Side,
    /// the position of room, horizontally or vertically.
    pub pos: usize,
    /// the number of door that is connected to this room
    pub num_connected_door: usize,
    /// the list of door this room connected to. There are one way connection (this should'nt influence the random path generation).
    pub linkedto: Vec<DoorReference>,
}

#[derive(Debug, PartialEq, Clone, Copy)]
/// a reference to another door in the grid
pub struct DoorReference {
    /// the id of the room the door is
    pub room_id: usize,
    /// the id of the connected door in the room
    pub door_id: usize,
}

impl Door {
    /// generate a new Door.
    ///
    /// side is the [`Side`] the door is
    /// pos is the coordinate the door is, based of the lower-left extremity of the room, and on the side.
    pub fn new(side: Side, pos: usize) -> Door {
        Door{side: side, pos: pos, num_connected_door: 0, linkedto: Vec::new()}
    }

    /// get absolute coordinate of the door in the [`Floor`]
    pub fn get_coordinate(&self, room_applied: &Room) -> Coordinate {
        Coordinate{
            x: match self.side {
                Side::Left => room_applied.x-1,
                Side::Right => room_applied.x + room_applied.xsize,
                Side::Down => room_applied.x + self.pos,
                Side::Up => room_applied.x + self.pos,
            },
            y: match self.side {
                Side::Left => room_applied.y + self.pos,
                Side::Right => room_applied.y + self.pos,
                Side::Down => room_applied.y - 1,
                Side::Up => room_applied.y + room_applied.ysize,
            }
        }
    }
}

impl RoomData {
    /// generate a new [`RoomData`], at the coordinate x, y, and of [`RoomType`] room
    fn new(x: usize, y: usize, room: RoomType) -> RoomData {
        RoomData {
            x:x,
            y:y,
            room_type: room,
            doors: Vec::new(),
            applied: Option::None,
        }
    }

    /// add random [`Door`] to this room
    pub fn add_doors_randomly(&mut self, rng: &mut ThreadRng) {
        let mut remaining_door_to_generate = match self.room_type {
            RoomType::High(_) | RoomType::Wide(_) => rng.gen_range(2,5),
            RoomType::Normal(s) => {
                match s {
                    2 => rng.gen_range(2,5),
                    _ => rng.gen_range(1,4),
                }
            },
        };

        if remaining_door_to_generate > 8 {
            error!("there are {} door to generate, that will loop indefinitely!!! Resetting this to 8.", remaining_door_to_generate);
            remaining_door_to_generate = 8;
        };

        let room_applied = match self.applied.as_ref() {
            None => {
                error!("the Room object is not generated. Skip the generation of doors for this room.");
                return;
            },
            Some(room) => room,
        };

        let mut door_by_side: [usize; 4] = [0,0,0,0];

        while remaining_door_to_generate > 0 {
            let selected_side = rng.gen_range(0,4);
            if door_by_side[selected_side] < 2 {
                door_by_side[selected_side] += 1;
                remaining_door_to_generate -= 1;
            };
        };

        for side in Side::iterator() {
            let index = match side {
                Side::Up => 0,
                Side::Down => 1,
                Side::Left => 2,
                Side::Right => 3,
            };
            let is_on_horizontal_wall = side.is_horizontal();
            let wall_size = match is_on_horizontal_wall {
                true => room_applied.xsize,
                false => room_applied.ysize,
            };
            let number_door_this_side = door_by_side[index];
            if number_door_this_side > 2 {
                panic!{"there are more than 2 door ({}) on a side ({:?}) !!!", number_door_this_side, side};
            };
            if number_door_this_side == 2 && wall_size == 3 {
                self.doors.push(Door::new(*side, 0));
                self.doors.push(Door::new(*side, 2));
            } else {
                if number_door_this_side >= 1 {
                    // place first door randomly
                    let first_door_position = rng.gen_range(0, wall_size);
                    self.doors.push(Door::new(*side, first_door_position));
                    if number_door_this_side == 2 {
                        if wall_size <= 3 {
                            error!("trying to generate two door on a door of less than 3 space. That is impossible. Skipping it.");
                            continue;
                        }
                        // place the second door
                        let mut possible_door_position: Vec<usize> = Vec::new();
                        for possible in 0..wall_size {
                            if !(
                                possible == first_door_position ||
                                possible + 1 == first_door_position ||
                                possible == first_door_position + 1
                            ) {
                                possible_door_position.push(possible);
                            }; //TODO: There won't be an exit on a side if it directly faces the border of the floor. May be unsolvable with 2 door and a space of 3 (or I don't understand it)
                        };
                        self.doors.push(Door::new(*side, rng.gen_range(0, possible_door_position.len())));
                    };
                };
            };
        }
    }

    /// randomly generate the [`Room`] object for this room
    pub fn generate(&mut self, room_size: usize, extra_room_space: usize, rng: &mut ThreadRng ) {
        fn compute_room_size(rng: &mut ThreadRng, size: usize, room_size: usize, extra_room_space: usize, position: usize) -> (usize, usize) {
            let max_size = size*(room_size+extra_room_space)-extra_room_space;
            let s = rng.gen_range(size*((room_size)/2), max_size);
            let max_additionable = max_size-s-1;
            (
                s,
                position*(room_size+extra_room_space)+match max_additionable {
                    0 => 0,
                    max_additionable => rng.gen_range(0,max_additionable)
                } + extra_room_space
            )
        };
        let (room_x_size, x_translation) = compute_room_size(rng, self.room_type.xsize(), room_size, extra_room_space, self.x);
        let (room_y_size, y_translation) = compute_room_size(rng, self.room_type.ysize(), room_size, extra_room_space, self.y);
        self.applied = Some(Room{
            x: x_translation,
            y: y_translation,
            xsize: room_x_size,
            ysize: room_y_size,
        });
    }

    /// apply this room on the [`Floor`]
    /// note: door for this room will also make space
    pub fn place(&self, level: &mut Floor) {
        let room_applied = self.applied.as_ref().expect("the  Room object is not generated.");
        for x in room_applied.x..room_applied.x+room_applied.xsize {
            for y in room_applied.y..room_applied.y+room_applied.ysize {
                let mut tile = Tile::new();
                tile.set_floor(FloorType::Path);
                level.level[x][y] = tile;
            }
        }

        for door in &self.doors {
            let mut tile = Tile::new();
            tile.set_floor(FloorType::Path);
            let coord = door.get_coordinate(room_applied);
            level.level[coord.x][coord.y] = tile;
        }
    }

}

// A struct that represent the floor as a grid of [`RoomData`]
pub struct RoomGrid {
    /// A grid that have true if a floor is on it, and false otherwise
    reserved: Vec<Vec<bool>>,
    /// the number of space it remain in this grid
    remaining_space: usize,
    /// the list of room in this grid
    pub rooms : Vec<RoomData>,
    /// the horizontal size of this grid
    pub xsize : usize,
    /// the vertical size of this grid
    pub ysize : usize,
}

#[derive(Debug)]
/// the list of error that can happen when placing a room
pub enum RoomPlaceError {
    /// can just not place the room
    CanNotPlaceRoom,
}

impl RoomGrid {
    /// generate a new [`RoomGrid`].
    ///
    /// xsize is for the horizontal size
    /// ysize is for the vertical size
    pub fn new(xsize: usize, ysize: usize) -> RoomGrid {
        RoomGrid {
            reserved: vec![vec![false; ysize]; xsize],
            rooms: Vec::new(),
            xsize: xsize,
            ysize: ysize,
            remaining_space: ysize*xsize,
        }
    }

    /// return the number of free tile in this grid
    pub fn remaining_space(&self) -> usize {
        self.remaining_space
    }

    /// return true if the pointed tile is occupied by a room, false otherwise
    pub fn is_reserved(&self, x: usize, y: usize) -> bool {
        self.reserved[x][y]
    }

    /// return a grid that can be used with a pathfinding algoritm
    ///
    /// room are considered as not passable, as well as their adjacent tile.
    /// going through a wall have a cost of 3, following a path, a cost of 1 (so the pathfinding algoritm try to reuse already existing path)
    /// It will also cost 6 to go throught wall at a distance of two tile of a room, and 4 at a distance of three tile of the room (so path are not usually near a room)
    /// None is umpassible, Some(cost) is possible with the cost 'cost'
    pub fn generate_pathinfing_grid(&self, level: &Floor, floor_x_size: usize, floor_y_size: usize) -> Vec<Vec<Option<usize>>> { //TODO: typo
        let new_wall_cost = 3;
        let follow_path_cost = 1;
        let mut grid = vec![vec![Some(999);floor_y_size];floor_x_size];

        for room in &self.rooms {
            let room_applied = match &room.applied {
                None => {
                    error!("There is an unaplied room used in the pathfinding ({:?}). Skipping it.", room);
                    continue;
                },
                Some(value) => value
            };

            for x in room_applied.x-1..room_applied.x+room_applied.xsize+1 {
                for y in room_applied.y-1..room_applied.y+room_applied.ysize+1 {
                    grid[x][y] = None;
                }
            }

            //prevent to go too near of a room when possible
            for x in room_applied.x-2..room_applied.x+room_applied.xsize+2 {
                for y in room_applied.y-2..room_applied.y+room_applied.ysize+2 {
                    match grid[x][y] {
                        Some(999) => {grid[x][y] = Some(6);},
                        _ => continue,
                    }
                }
            }

            for x in room_applied.x-3..room_applied.x+room_applied.xsize+3 {
                for y in room_applied.y-3..room_applied.y+room_applied.ysize+3 {
                    match grid[x][y] {
                        Some(999) => {grid[x][y] = Some(4);},
                        _ => continue,
                    }
                }
            }

            for door in &room.doors {
                let coord = door.get_coordinate(room_applied);
                grid[coord.x][coord.y] = Some(new_wall_cost);
            }
        }

        for x in 0..floor_x_size {
            for y in 0..floor_y_size {
                match grid[x][y] {
                    None => continue,
                    Some(999) => grid[x][y] = Some(
                        match level.level[x][y].floor {
                            FloorType::Wall => new_wall_cost,
                            FloorType::Path => follow_path_cost,
                            non_normal_tile => {
                                error!("A floor of type {:?} is found in the grid generation for the pathfinding algoritm for the world generation and is unexpected!!! Assigning it a cost of a normal wall.", non_normal_tile);
                                new_wall_cost
                            }
                        }
                    ),
                    Some(_) => {
                        match level.level[x][y].floor {
                            FloorType::Path => grid[x][y] = Some(follow_path_cost),
                            _ => continue,
                        }
                    },
                }
            }
        }

        grid
    }

    /// check if the room `data` can be placed
    pub fn can_room_be_placed(&self, data: &RoomData) -> bool {
        if (data.x + data.room_type.xsize() > self.xsize) || (data.y + data.room_type.ysize() > self.ysize) {
            return false
        }
        for x in data.x..data.x+data.room_type.xsize() {
            for y in data.y..data.y+data.room_type.ysize() {
                if self.is_reserved(x, y) {
                    return false;
                };
            }
        }
        true
    }

    /// create a room of the type `room`, and place it at the coordinate xy in the room grid
    pub fn add_room(&mut self, x: usize, y: usize, room: RoomType) -> Result<usize, RoomPlaceError> {
        let data = RoomData::new(x, y, room);
        self.add_room_data(data)
    }

    /// place a `RoomData` in the room grid
    pub fn add_room_data(&mut self, data: RoomData) -> Result<usize, RoomPlaceError> {
        if !self.can_room_be_placed(&data) {
            return Err(RoomPlaceError::CanNotPlaceRoom)
        }
        for x in data.x..data.x+data.room_type.xsize() {
            for y in data.y..data.y+data.room_type.ysize() {
                self.reserved[x][y] = true;
            }
        }
        self.remaining_space = self.remaining_space - data.room_type.xsize()*data.room_type.ysize();
        self.rooms.push(data);
        Ok(self.rooms.len()-1)
    }

    pub fn place_big_room(&mut self, mut rng: &mut ThreadRng, luck_of_big_room: f64, luck_reduce_2_in_1: f64) {
        #[derive(PartialEq, Debug)]
        /// containt the type of the big room that will be generated
        enum WideOrHigh {
            Wide,
            High,
            None,
        }
        let mut max_possible_high_room = match self.xsize {
            x if x >= 5 => 2,
            x if x >= 2 => 1,
            _ => 0,
        };
        let mut max_possible_wide_room = match self.ysize {
            y if y >= 5 => 2,
            y if y >= 2 => 1,
            _ => 0,
        };
        let wide_or_high = match max_possible_high_room {
            0 => match max_possible_wide_room {
                0 => WideOrHigh::None,
                _ => match rng.gen_bool(luck_of_big_room) {
                    false => WideOrHigh::None,
                    true => WideOrHigh::Wide,
                },
            },
            _ => match max_possible_wide_room {
                0 => match rng.gen_bool(luck_of_big_room) {
                    false => WideOrHigh::None,
                    true => WideOrHigh::High,
                },
                _ => match rng.gen_bool(luck_of_big_room) {
                    false => WideOrHigh::None,
                    true => match rng.gen_bool(0.5) {
                        true => WideOrHigh::Wide,
                        false => WideOrHigh::High,
                    },
                },
            },
        };
        if max_possible_high_room == 2 {
            max_possible_high_room = match rng.gen_bool(luck_reduce_2_in_1) { true => 1, false => 2};
        };

        if max_possible_wide_room == 2 {
            max_possible_wide_room = match rng.gen_bool(luck_reduce_2_in_1) { true => 1, false => 2};
        };

        fn compute_center_column(max_room_number: usize, rng: &mut ThreadRng) -> usize {
            match rng.gen_range(0,3) {
                0 => match max_room_number%2 {
                    0 => max_room_number/2-1,
                    _ => max_room_number/2+rng.gen_range(0,1),
                },
                1 => max_room_number - 2,
                2 => 0,
                _ => panic!()
            }
        }

        match wide_or_high {
            WideOrHigh::None => (),
            WideOrHigh::High => {
                match max_possible_high_room {
                    1 => {
                        self.add_room(compute_center_column(self.xsize, &mut rng), 0, RoomType::High(self.ysize))
                            .unwrap();
                    },
                    2 => {
                        self.add_room(0, 0, RoomType::High(self.ysize))
                        .unwrap();
                        self.add_room(self.xsize-2, 0, RoomType::High(self.ysize))
                        .unwrap();
                    },
                    _ => panic!(),
                }
            },
            WideOrHigh::Wide => {
                match max_possible_wide_room {
                    1 => {
                        self.add_room(0, compute_center_column(self.ysize, &mut rng), RoomType::Wide(self.xsize))
                        .unwrap();
                    },
                    2 => {
                        self.add_room(0, 0, RoomType::Wide(self.xsize))
                        .unwrap();
                        self.add_room(0, self.ysize-2, RoomType::Wide(self.xsize))
                        .unwrap();
                    }
                    _ => panic!(),
                }
            },
        };


    }

    /// place a room of the type `room` randomly in the room grid
    pub fn place_room_random(&mut self, room: RoomType, rng: &mut ThreadRng) -> Option<usize> {
        let mut data = RoomData::new(0, 0, room);
        let mut possible_selection: Vec<Coordinate> = Vec::new();
        for x in 0..self.xsize {
            data.x = x;
            for y in 0..self.ysize {
                data.y = y;
                if self.can_room_be_placed(&data) {
                    possible_selection.push(Coordinate{x:x, y:y});
                }
            }
        }
        match possible_selection.len() {
            0 => None,
            _ => {
                let chosen = rng.gen_range(0, possible_selection.len());
                data.x = possible_selection[chosen].x;
                data.y = possible_selection[chosen].y;
                Some(self.add_room_data(data).unwrap())
            }
        }
    }

    /// Return the room at the coordinate xy, or None if no such one exist
    pub fn room_at(&self, x:usize, y:usize) -> Option<&RoomData> {
        if !self.is_reserved(x,y) {return None;};
        for room in &self.rooms {
            if x >= room.x && x < room.x+room.room_type.xsize() && y >= room.y && y < room.y+room.room_type.ysize() {
                return Some(room);
            };
        };
        error!("room at x:{},y:{} reserved but no room is found for it.", x, y);
        None
    }

    /// create link betweens doors in rooms
    pub fn generate_door_link(&mut self, rng: &mut ThreadRng) {
        fn room_has_remaining_free_door(room: &RoomData) -> bool {
            let mut has_remaining_door = false;
            for door in &room.doors {
                if door.num_connected_door < 2 {
                    has_remaining_door = true;
                };
            }
            return has_remaining_door;
        };
        let mut room_id_with_access = Vec::new();
        room_id_with_access.push(rng.gen_range(0, self.rooms.len()));

        while room_id_with_access.len() < self.rooms.len() {
            let first_room_id = room_id_with_access[rng.gen_range(0, room_id_with_access.len())];
            if !room_has_remaining_free_door(&self.rooms[first_room_id]) {
                continue;
            };

            let mut free_doors = Vec::new();
            for door_id in 0..self.rooms[first_room_id].doors.len() {
                if self.rooms[first_room_id].doors[door_id].num_connected_door < 2 {
                    free_doors.push(door_id);
                }
            };

            let first_door_id = free_doors[rng.gen_range(0,free_doors.len())];

            let mut lone_rooms_id = Vec::new(); //TODO: check if there is a standard function for the following

            for room_id in 0..self.rooms.len() {
                let mut put = true;
                for done_room_id in &room_id_with_access {
                    if *done_room_id == room_id {
                        put = false;
                    };
                }
                if put {
                    lone_rooms_id.push(room_id);
                };
            }

            let second_room_id = lone_rooms_id[match lone_rooms_id.len() {
                0 => 0,
                len => rng.gen_range(0, len),
            }];

            let second_door_id = rng.gen_range(0,self.rooms[second_room_id].doors.len());

            self.rooms[first_room_id].doors[first_door_id].linkedto.push(DoorReference{room_id: second_room_id, door_id: second_door_id});
            self.rooms[first_room_id].doors[first_door_id].num_connected_door += 1;
            self.rooms[second_room_id].doors[second_door_id].num_connected_door += 1;

            room_id_with_access.push(second_room_id);
        }


        fn get_door_with_up_to_x_connection(grid: &RoomGrid, x: usize) -> Vec<DoorReference> { //TODO: replace _up_to_ with a lambda
            let mut result = Vec::new();
            for room_id in 0..grid.rooms.len() {
                for door_id in 0..grid.rooms[room_id].doors.len() {
                    if grid.rooms[room_id].doors[door_id].num_connected_door <= x {
                        result.push(DoorReference{room_id: room_id, door_id: door_id});
                    }
                }
            }
            result
        };

        while get_door_with_up_to_x_connection(&self, 0).len() > 0 {
            let unconnected_door = get_door_with_up_to_x_connection(&self, 0);
            let to_connect_door = get_door_with_up_to_x_connection(&self, 1);

            let mut second_door = to_connect_door[rng.gen_range(0, to_connect_door.len())];
            if to_connect_door.len() == 1 {
                let all_doors = get_door_with_up_to_x_connection(&self, 2);
                if all_doors.len() == 1 {
                    break;
                };
                error!("there is a door that can't be connected to another one. It will be connected to to a door that will have three connections. This is undesirable but it is hard to write a good algoritm for this.");
                second_door = all_doors[rng.gen_range(0, all_doors.len())];
            }
            let first_door = unconnected_door[rng.gen_range(0, unconnected_door.len())];

            if first_door == second_door {
                continue;
            }

            self.rooms[second_door.room_id].doors[second_door.door_id].num_connected_door += 1;
            self.rooms[first_door.room_id].doors[first_door.door_id].num_connected_door += 1;
            self.rooms[first_door.room_id].doors[first_door.door_id].linkedto.push(second_door);
        }
    }

    /// place the corridor between linked doors. Use a pathfinding algoritm to do so.
    pub fn place_corridor(&self, level: &mut Floor) {
        for room in &self.rooms {
            for door in &room.doors {
                let start = door.get_coordinate(room.applied.as_ref().unwrap());

                for connection in &door.linkedto {
                    let grid = self.generate_pathinfing_grid(&level, level.xsize, level.ysize);
                    let end_room = &self.rooms[connection.room_id];
                    let end_door = &end_room.doors[connection.door_id];
                    let end = end_door.get_coordinate(end_room.applied.as_ref().unwrap());

                    let path = find_path(&grid, start, end);
                    match path {
                        Some(path) => for p in path {
                            let mut tile = Tile::new();
                            tile.set_floor(FloorType::Path);
                            level.level[p.x][p.y] = tile;
                        },
                        None => error!("a path was unable to be computed!!! Ignoring it."),
                    };
                }
            }
        }
    }

    /// display the room grid in the terminal
    ///
    /// | is for High room, - for Wide room, 1 for Normal(1), 2 for Normal(2), + for others Normal
    pub fn display(&self) {
        for y in 0..self.ysize {
            let mut l = String::new();
            for x in 0..self.xsize {
                l.push_str(match self.room_at(x, y) {
                    None => "x",
                    Some(value) => match value.room_type {
                        RoomType::High(_) => "|",
                        RoomType::Wide(_) => "-",
                        RoomType::Normal(size) => match size {
                            1 => "1",
                            2 => "2",
                            _ => "+",
                        },
                    }
                });
            }
            println!("{}", l);
        }
    }
}
