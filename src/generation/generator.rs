use crate::dungeon::floor::Floor;

/// a random generator for a [`Floor`] of a dungeon
pub trait Generate {
    fn generate(&self) -> Floor;
}
