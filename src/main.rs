#[macro_use]
extern crate log;
extern crate env_logger;

use dungeongeneration::generation::random_generation::RandomGeneration;
use dungeongeneration::generation::generator::Generate;

fn main() {
    env_logger::init();

    info!("logger started");

    info!("testing by generating a lot of dungeon");
    for _ in 0..0 {
        RandomGeneration::new(40, 40).generate();
    }
    info!("producing good looking dungeon");
    for _ in 0..1 {
        RandomGeneration::new(50, 50).generate().display();
        println!("------------------------------");
    }
    info!("program finished");
}
