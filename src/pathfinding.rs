use crate::common::{Coordinate, Side};
use std::ops::{Index, IndexMut};
//TODO: put in another crate

struct DynamicVec<T> {
    vec: Vec<T>,
    default: T,
}

impl Index<usize> for DynamicVec<Vec<Coordinate>> { //TODO: find how to replace usize by something generic and type generic
    type Output = Vec<Coordinate>;

    fn index(&self, id: usize) -> &Vec<Coordinate> {
        match self.vec.len() > id {
            true => &self.vec[id],
            false => &self.default,
        }
    }
}

impl IndexMut<usize> for DynamicVec<Vec<Coordinate>> {
    fn index_mut(&mut self, id: usize) -> &mut Vec<Coordinate> {
        match self.vec.len() > id {
            true => &mut self.vec[id],
            false => {
                while !(self.vec.len() > id) {
                    self.vec.push(self.default.clone());
                }
                &mut self.vec[id]
            }
        }
    }
}

/// a simple pathfinding algoritm for 2d grid, that find the shortest path, with movement only possible up, down, left and right
///
/// the zero coordinate is at the down-left side, and grow by going up/right
///
/// the grid is a reference to a Vector of Vector of Option of usize. If the option is None, the tile if unpassable. If it is Some<usize>, it have the cost of the usize
/// the start and the end point are [`Coordinate`]
///
/// It will return Some<Vec<Coordinate>>, that will be None if no path is found, else it is the list of Coordinate needed to access the end point from the start point
//TODO: test
pub fn find_path(grid: &Vec<Vec<Option<usize>>>, start: Coordinate, end: Coordinate) -> Option<Vec<Coordinate>>{

    let xsize = grid.len();
    if xsize == 0 {
        return None;
    }
    let ysize = grid[0].len();
    let mut path_grid: Vec<Vec<Option<Side>>> = vec![vec![None; ysize]; xsize]; // 0 is the lower possible cost

    let mut finished = false;
    let mut path_list = DynamicVec {
        vec: vec![vec![start.clone()]],
        default: vec![],
    };
    let mut loop_nb = 0;
    while !finished {
        for path in path_list[loop_nb].clone() {
            for side in Side::iterator() {
                // determine if this is a legal move:
                if !side.is_legal(path, xsize, ysize) {
                    continue;
                };
                // compute destination coordinate
                let dest_coord = Coordinate {
                    x: (path.x as isize + side.x_movement()) as usize,
                    y: (path.y as isize + side.y_movement()) as usize,
                };
                // check it doesnt overwrite an alredy existing path and that it is avalaible
                if path_grid[dest_coord.x][dest_coord.y].is_some() {
                    continue;
                };
                let mut cost = match grid[dest_coord.x][dest_coord.y] {
                    None => continue,
                    Some(cost) => cost,
                };

                // register the new possible way
                path_grid[dest_coord.x][dest_coord.y] = Some(side.inverse());
                if dest_coord == end {
                    finished = true;
                };
                if cost == 0 {
                    error!("in the pathfinding, a tile have a coost of 0!!! Replacing it with a cost of 1.");
                    cost = 1;
                };
                path_list[loop_nb+cost].push(dest_coord);
            }
        }
        loop_nb += 1;
    }

    match path_grid[end.x][end.y] {
        None => None,
        Some(_) => {
            let mut actual_coordinate = end;
            let mut reverse_path = Vec::new();
            while actual_coordinate != start {
                let dest_side = path_grid[actual_coordinate.x][actual_coordinate.y];
                actual_coordinate = Coordinate{
                    x: (actual_coordinate.x as isize + dest_side.unwrap().x_movement()) as usize,
                    y: (actual_coordinate.y as isize + dest_side.unwrap().y_movement()) as usize,
                };
                reverse_path.push(actual_coordinate);
            }
            let mut path = Vec::new();
            while reverse_path.len() > 0 {
                path.push(reverse_path.pop().unwrap());
            }
            Some(path)
        }
    }
}

pub fn test_accessibility(grid: &Vec<Vec<bool>>, start: &Coordinate) -> Vec<Vec<bool>> {
    let ysize = grid[0].len();
    let xsize = grid.len();
    let mut result = vec![vec![false; ysize]; xsize];
    result[start.x][start.y] = true;
    let mut coordinate_to_do = Vec::new();
    coordinate_to_do.push(start.clone());
    while coordinate_to_do.len() != 0 {
        let actual_coordinate = match coordinate_to_do.pop() {
            None => {
                error!("impossible to unpack coordinate_to_do in test_accessibility (seriously, that shouln't happen since I just previously tested if it's len wasn't 0)!!! skipping it.");
                continue;
            },
            Some(x) => x,
        };
        for side in Side::iterator() {
            if !side.is_legal(actual_coordinate, xsize, ysize) {
                continue;
            };
            let new_coordinate = Coordinate {
                x: (actual_coordinate.x as isize + side.x_movement()) as usize,
                y: (actual_coordinate.y as isize + side.y_movement()) as usize,
            };
            match grid[new_coordinate.x][new_coordinate.y] {
                true => {
                    match result[new_coordinate.x][new_coordinate.y] {
                        false => {
                            result[new_coordinate.x][new_coordinate.y] = true;
                            coordinate_to_do.push(new_coordinate);
                        },
                        true => { },
                    }
                },
                false => {},
            };
        }
    }
    result
}
